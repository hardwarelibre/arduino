#include <Servo.h>
#include <RCSwitch.h>
#include <NewPing.h>

unsigned long serialdata;
int inbyte;

int pinNumber;
int sensorVal;
int analogRate;
int digitalState;

int led = 13;
int blinkdelay = 1;

// Shift registers
int srd;
int srl;
int src;
int srdata;

// rf 433
RCSwitch mySwitch = RCSwitch();
int rf433Pin;
int rf433Data1;
int rf433Data2;

// newPing
#define MAX_DISTANCE 300 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

// Adafruit Motor Shield V2 - DC Motors
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);

// Servos
Servo Servo1;
Servo Servo2;

void setup()
{
  Serial.begin(9600);
  //Serial.begin(38400);
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  
  AFMS.begin();  // create with the default frequency 1.6KHz
  Servo1.attach(9);
  Servo2.attach(10);
}

void blinkLED(byte targetPin, int numBlinks, int blinkRate) {
  for (int i=0; i < numBlinks; i++) {
    digitalWrite(targetPin, HIGH);   // sets the LED on
    delay(blinkRate);                     // waits for blinkRate milliseconds
    digitalWrite(targetPin, LOW);    // sets the LED off
    delay(blinkRate);
  }
}

void tobin(int valeur, int digit, int nombre[])
{
      int i;
      int temp;
      
      temp=valeur;
      for (i=0;i<digit;i++){   // itère pour le nombre de digit attendus
            nombre[i]=temp & 1;  // prend le LSB et le sauve dans nombre
            temp = temp >> 1;    // décalage d'un bit sur la droite
      }
}

void loop()
{
  getSerial();
  blinkLED(led, 1, blinkdelay); 
  int SerialData1 = serialdata;
  switch(SerialData1)
  {
  // PIN WRITE
  case 1:
    {
      //blinkLED(led, 1, blinkdelay);
      //analog digital write
      getSerial();
      int SerialData2 = serialdata;
      switch (SerialData2)
      {
      case 1:
        {
          //analog write
          getSerial();
          pinNumber = serialdata;
          getSerial();
          analogRate = serialdata;
          pinMode(pinNumber, OUTPUT);
          analogWrite(pinNumber, analogRate);        
          pinNumber = 0;
          break;
        }
      case 2:
        {
          //digital write
          getSerial();
          pinNumber = serialdata;
          getSerial();
          digitalState = serialdata;
          pinMode(pinNumber, OUTPUT);
          if (digitalState == 0)
          {
            digitalWrite(pinNumber, LOW);
          }
          if (digitalState == 1)
          {
            digitalWrite(pinNumber, HIGH);
          }
          pinNumber = 0;
          break;
         
        }
     }
     break; 
    }
    // PIN READ
    case 2:
    {
      //blinkLED(led, 1, blinkdelay);
      getSerial();
      int SerialData2 = serialdata;
      switch (SerialData2)
      {
      case 1:
        {
          //digital read
          getSerial();
          pinNumber = serialdata;
          pinMode(pinNumber, INPUT);
          sensorVal = digitalRead(pinNumber);
          Serial.println(sensorVal);
          sensorVal = 0;
          pinNumber = 0;
          break;
        }
      case 2:
        {
          //analog read
          getSerial();
          pinNumber = serialdata;
          pinMode(pinNumber, INPUT);
          sensorVal = analogRead(pinNumber);
          Serial.println(sensorVal);
          sensorVal = 0;
          pinNumber = 0;
          break;
        } 
      }
      break;
    }
    // SERVO
    case 3:
    {
      //blinkLED(led, 1, blinkdelay);
      getSerial();
      int servo_num = serialdata;
      switch (servo_num)
      {
        case 1:
        {
          getSerial();
          int servoPos = serialdata;
          Servo1.write(servoPos);
          break;
        }
        case 2:
        {
          getSerial();
          int servoPos = serialdata;
          Servo2.write(servoPos);
          break;
        }
      }
      break;
    }
    // Shift register
    case 4:
    {
      //blinkLED(led, 1, blinkdelay);
      getSerial();
      srd = serialdata;
      getSerial();
      srl = serialdata;
      getSerial();
      src = serialdata;
      getSerial();
      srdata = serialdata;
      pinMode(srd, OUTPUT);
      pinMode(srl, OUTPUT);
      pinMode(src, OUTPUT);
      digitalWrite(srl, LOW);         
      shiftOut(srd, src, MSBFIRST, srdata); 
      digitalWrite(srl, HIGH); 
      // Serial.println(srdata);
      break;
    }
    // prise RF433 send
    case 5:
    {
      //blinkLED(led, 1, blinkdelay);
      getSerial();
      rf433Pin = serialdata;
      Serial.println(rf433Pin);
      getSerial();
      rf433Data1 = serialdata;
      Serial.println(rf433Data1);
      getSerial();
      rf433Data2 = serialdata;
      Serial.println(rf433Data2);
      getSerial();
      mySwitch.enableTransmit(rf433Pin);
      int SerialData2 = serialdata;
      switch (SerialData2)
      {
        case 0:
        {
          mySwitch.switchOff(rf433Data1, rf433Data2);
        }
        case 1:
        {
          mySwitch.switchOn(rf433Data1, rf433Data2);
        }
      }
      break;
    }
    // HC-SR04
    case 6:
    {
      getSerial();
      int trigPin = serialdata;
      getSerial();
      int echoPin = serialdata;
      long lecture_echo; 
      // init pins
      pinMode(trigPin, OUTPUT); 
      digitalWrite(trigPin, LOW); 
      pinMode(echoPin, INPUT); 
      
      digitalWrite(trigPin, HIGH); 
      delayMicroseconds(10); 
      digitalWrite(trigPin, LOW);
      lecture_echo = pulseIn(echoPin, HIGH); 
      Serial.println(lecture_echo);
      break;
    }
    // DC Motor
    case 7:
    {
      getSerial();
      int pwmPin = serialdata;
      getSerial();
      int pwmVar = serialdata;
      pinMode(pwmPin, OUTPUT);
      analogWrite(pwmPin, pwmVar);
      break;
    }
    // HC-SR04 (new)
    case 8:
    {
      getSerial();
      int trigPin = serialdata;
      getSerial();
      int echoPin = serialdata;
      // long lecture_echo; 
      NewPing sonar(trigPin, echoPin, MAX_DISTANCE); // NewPing setup of pins and maximum distance.     
      unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS).
      
      //Serial.println(uS / US_ROUNDTRIP_CM);
      Serial.println(uS);
      break;
    }
    // Adafruit Motor Shield V2 - DC Motors
    case 9:
    {
      getSerial();
      int motor_number = serialdata;
      getSerial();
      int motor_direction = serialdata;   
      getSerial();
      int motor_speed = serialdata; 
      switch (motor_number)
      {
        case 1:
        {
        switch (motor_direction)
          {
            case 0:
            {
              Motor1-> run(RELEASE);
              break;
            }
            case 1:
            {
              Motor1-> run(FORWARD);
              break;
            }
            case 2:
            {
              Motor1-> run(BACKWARD);
              break;
            }
          }
        Motor1->setSpeed(motor_speed);
        break;
        }
        case 2:
        {
        switch (motor_direction)
          {
            case 0:
            {
              Motor2-> run(RELEASE);
              break;
            }
            case 1:
            {
              Motor2-> run(FORWARD);
              break;
            }
            case 2:
            {
              Motor2-> run(BACKWARD);
              break;
            }
          }
        Motor2->setSpeed(motor_speed);
        break;
        }
      }
      break;
    }
    // IR collision
    /******************************************************************************
     * This function can be used with a panasonic pna4602m ir sensor
     * it returns a zero if something is detected by the sensor, and a 1 otherwise
     * The function bit bangs a 38.5khZ waveform to an IR led connected to the
     * triggerPin for 1 millisecond, and then reads the IR sensor pin to see if
     * the reflected IR has been detected
     ******************************************************************************/
    //case 9:
    //{
    //  getSerial();
    //  int irLedPin = serialdata;
    //  getSerial();
    //  int irSensorPin = serialdata;
    //  int irRead(int readPin, int triggerPin);
    //  pinMode(irSensorPin, INPUT);
    //  pinMode(irLedPin, OUTPUT);
  
    //  Serial.println(irRead(irSensorPin, irLedPin)); //display the results
    //  break;
    //}
    // Sharp GP2Y0A02YK0F - https://www.inkling.com/read/arduino-cookbook-michael-margolis-2nd/chapter-6/figure-6-6
    //case 10:
    //{
    //  getSerial();
    //  int ledPin = serialdata;
    //  getSerial();
    //  int sensorPin = serialdata;
    //  long referenceMv = 5000; // long int to prevent overflow when multiplied
    //  pinMode(ledPin, OUTPUT);
    //  int val = analogRead(sensorPin);
    //  int mV = (val * referenceMv) / 1023;
    //  int cm = getDistance(mV);
    //  Serial.println(cm);
    //  break;
    //}
      // Identification
    case 42:
    {
      //blinkLED(led, 3, 10);
      Serial.println("arduino");
      break;
    }
  }
  delay(1);
}

long getSerial()
{
  serialdata = 0;
  while (inbyte != '/')
  {
    inbyte = Serial.read(); 
    // inbyte = Serial.read()-'0';
    if (inbyte > 0 && inbyte != '/')
    {
     
      serialdata = serialdata * 10 + inbyte - '0';
    }
  }
  inbyte = 0;
  return serialdata;
}

/******************************************************************************
 * This function can be used with a panasonic pna4602m ir sensor
 * it returns a zero if something is detected by the sensor, and a 1 otherwise
 * The function bit bangs a 38.5khZ waveform to an IR led connected to the
 * triggerPin for 1 millisecond, and then reads the IR sensor pin to see if
 * the reflected IR has been detected
 ******************************************************************************/
int irRead(int readPin, int triggerPin)
{
  int halfPeriod = 13; //one period at 38.5khZ is aproximately 26 microseconds
  int cycles = 38; //26 microseconds * 38 is more or less 1 millisecond
  int i;
  for (i=0; i <=cycles; i++)
  {
    digitalWrite(triggerPin, HIGH); 
    delayMicroseconds(halfPeriod);
    digitalWrite(triggerPin, LOW); 
    delayMicroseconds(halfPeriod - 1);     // - 1 to make up for digitaWrite overhead    
  }
  return digitalRead(readPin);
}

// Sharp GP2Y0A02YK0F - https://www.inkling.com/read/arduino-cookbook-michael-margolis-2nd/chapter-6/figure-6-5
// the following is used to interpolate the distance from a table
// table entries are distances in steps of 250 millivolts
const int TABLE_ENTRIES = 12;
const int firstElement = 250; // first entry is 250 mV
const int INTERVAL  = 250; // millivolts between each element
static int distance[TABLE_ENTRIES] = {150,140,130,100,60,50,40,35,30,25,20,15};

int getDistance(int mV)
{
   if( mV >  INTERVAL * TABLE_ENTRIES-1 )
      return distance[TABLE_ENTRIES-1];
   else
   {
      int index = mV / INTERVAL;
      float frac = (mV % 250) / (float)INTERVAL;
      return distance[index] - ((distance[index] - distance[index+1]) * frac);
   }
}
